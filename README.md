Basic k8s Cluster Configuration
===

Prerequisite
---

Have the cluster config added to `$HOME/.kube/config` and set as default context. The config content
can be found in Rancher GUI, in the dashboard for the cluster, button *Kubeconfig File*.


Prepare your Local Machine
---

Install ansible roles:

```
cd ansible
ansible-galaxy collection install -r roles/requirements.yml
```

Install tools from `ansible` directory on local machine; as *root*, use sudo or root account (without sudo):

```
sudo ansible-playbook -l localhost pb_controlhost.yml
```
See the ansible subdirectory README for instructions to install as non root user.


Configure Rancher CLI
---

Rancher projects cannot be declared, they have to be configured via GUI or CLI. The next playbook will
use the Rancher CLI to configure some basic projects. For a CLI login a
bearer token has to used. In the Rancher GUI, go to *User / API & Keys* and create an access key.
Login into Rancher CLI:

```
rancher --token <bearer-token> login https://ids-rnc.ethz.ch
```

Rancher CLI can now create projects.


Prepare Kubernetes Cluster
---

The following playbook will create the namespace for ArgoCD, install the SealedSecret service and
creates the Rancher projects defined in `host_vars/localhost.yml`.

```
ansible-playbook -l localhost pb_k8s.yml
```


Install ArgoCD
---

Apply ArgoCD configuration in cluster. Change to directory corresponding to the desired cluster instance,
ie. *k8s-argocd/tst*. Then apply with

```
kustomize build . | kubectl apply -f -
```


Change ArgoCD Password
---

User *admin* initially has the password set to the 1st server container. Change with:

```
kubectl get pods -n argocd | grep argocd-server | cut -d " " -f 1
argocd login 172.31.91.7:30000 --insecure --username admin --password $(kubectl get pods -n argocd | grep argocd-server | cut -d " " -f 1)
argocd account update-password
```

